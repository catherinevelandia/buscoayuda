from unittest import  TestCase
from selenium import webdriver

class FunctionalTest(TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

    def test_register(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('linkRegistrarse')
        link.click()

        nombre = self.browser.find_element_by_id('name')
        nombre.send_keys('Luis')

        apellidos = self.browser.find_element_by_id('last_name')
        apellidos.send_keys('Arango')

        aniosExperiencia = self.browser.find_element_by_id('years_of_experience')
        aniosExperiencia.send_keys('4')

        telefono = self.browser.find_element_by_id('phone_number')
        telefono.send_keys('3111111')

        correoElectronico = self.browser.find_element_by_id('email')
        correoElectronico.send_keys('pacho5@buscoayuda.com')

        contrasenia = self.browser.find_element_by_id('password')
        contrasenia.send_keys('clave1234')

        usuario = self.browser.find_element_by_id('username')
        usuario.send_keys('luis')

        foto = self.browser.find_element_by_id('imageFileUrl')
        foto.send_keys('http://es.fakenamegenerator.com/images/sil-male.png')

        botonGuardar = self.browser.find_element_by_id('butGuardar')
        botonGuardar.click()

        self.browser.implicitly_wait(5)
        p = self.browser.find_element_by_xpath("//p[text()='Luis Arango']")

        self.assertIn('Luis Arango', p.text)


    def test_detalle(self):
        self.browser.get('http://localhost:8000')

        self.browser.implicitly_wait(3)
        independiente = self.browser.find_element_by_id('idBuscarIndependiente')
        independiente.send_keys('luis')

        botonBuscar = self.browser.find_element_by_id('butBuscar')
        botonBuscar.click()

        self.browser.implicitly_wait(5)
        nombre = self.browser.find_element_by_xpath("//p[text()='Luis Arango']")

        self.assertIn('Luis Arango', nombre.text)